﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Bonus_N01331213
{
    public partial class PalindromeOrNot : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        //Multiple pages from this guide were referenced in making this https://docs.microsoft.com/en-us/dotnet/csharp/index
        protected void PalindromeClick(object sender, EventArgs e)
        {
            string inputWord = userWordInput.Text.ToString();
            string lowerWord = inputWord.ToLower();
            string noSpaceWord = lowerWord.Replace(" ", string.Empty);
            string reverseWord = "";

            for (int i = noSpaceWord.Length - 1; i >= 0; i--)
            {
                reverseWord = reverseWord + noSpaceWord[i];
            }

            if (noSpaceWord == reverseWord)
            {
                palindromeInfo.InnerHtml = inputWord + " is a palindrome";
            }
            else
            {
                palindromeInfo.InnerHtml = inputWord + " is not a palindrome";
            }
        }      
    }
}
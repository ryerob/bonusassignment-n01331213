﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Bonus_N01331213
{
    public partial class FineTheQuadrant : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

        protected void QuadrantClick(object sender, EventArgs e)
        {
            int xAxis = int.Parse(xAxisInput.Text);
            int yAxis = int.Parse(yAxisInput.Text);

            if (xAxis > 0 && yAxis > 0)
            {
                quadrantOutput.InnerHtml = "Quadrant 1";
            }
            else if (xAxis > 0 && yAxis < 0)
            {
                quadrantOutput.InnerHtml = "Quadrant 4";
            }
            else if (xAxis < 0 && yAxis < 0)
            {
                quadrantOutput.InnerHtml = "Quadrant 3";
            }
            else if (xAxis < 0 && yAxis > 0)
            {
                quadrantOutput.InnerHtml = "Quadrant 2";
            }
        }

    }
}
﻿<%@ Page Language="C#"  AutoEventWireup="true" CodeBehind="PalindromeOrNot.aspx.cs" Inherits="Bonus_N01331213.PalindromeOrNot" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Palidrome or Not</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>String Bling</h1>
            <h2>Enter a word or phrase to see if it is a palindrome.</h2>
            <label>Enter Word: </label>
            <asp:TextBox runat="server" ID="userWordInput"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a word." ControlToValidate="userWordInput" ID="RequiredFieldValidator1" />
            <asp:RegularExpressionValidator runat="server" ID="onlyLettersValidation" ControlToValidate="userWordInput" ErrorMessage="Enter a word with only letters." ValidationExpression="^[a-zA-Z]+(\s[a-zA-Z]+)?$"></asp:RegularExpressionValidator>
          
            <%-- Validation expression found at https://www.codeproject.com/Questions/492512/IplusneedplusaplusRegularplusexpressionplusforplus --%>

            <br />
            <asp:Button Text="Submit" runat="server" OnClick="PalindromeClick" ID="palidromeButton" />
            <hr />
            <div id="palindromeInfo" runat="server">

            </div>
        </div>
    </form>
</body>
</html>

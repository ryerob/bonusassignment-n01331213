﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FindTheQuadrant.aspx.cs" Inherits="Bonus_N01331213.FineTheQuadrant" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>

    <%--Validation expression found at regexlib.com  --%>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Cartesian Smartesian</h1>
            <h2>Enter a x and y axis to find which quadrant on an axis it is.</h2>
            <label>x-axis: </label>
            <asp:TextBox runat="server" ID="xAxisInput"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a number for the x axis." ControlToValidate="xAxisInput" ID="RequiredFieldValidator1" />
            <asp:RegularExpressionValidator runat="server" ID="onlyNumericValidatorX" ControlToValidate="xAxisInput" ErrorMessage="Enter a valid number." ValidationExpression="^(\+|-)?\d+$"></asp:RegularExpressionValidator>
            
            <label>y-axis: </label>
            <asp:TextBox runat="server" ID="yAxisInput"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a number for the y axis." ControlToValidate="yAxisInput" ID="RequiredFieldValidator2" />
            <asp:RegularExpressionValidator runat="server" ID="onlyNumericValidatorY" ControlToValidate="yAxisInput" ErrorMessage="Enter a valid number." ValidationExpression="^(\+|-)?\d+$"></asp:RegularExpressionValidator>
            <br />
            <br />
            <asp:Button Text="Submit" runat="server" OnClick="QuadrantClick" ID="quadrantBtn" />      
        </div>
        <hr />
        <div id="quadrantOutput" runat="server">

        </div>
    </form>
</body>
</html>

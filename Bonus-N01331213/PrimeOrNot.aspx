﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrimeOrNot.aspx.cs" Inherits="Bonus_N01331213.IsNumberPrime" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Divisible Sizzable</h1>
            <h2>Enter a number to find out if it is prime or not.</h2>
            <label>Enter a Number: </label>
            <asp:TextBox runat="server" ID="primeUserInput"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a number." ControlToValidate="primeUserInput" ID="RequiredFieldValidator1" />
            <asp:RegularExpressionValidator runat="server" ID="onlyNumericValidatorPrime" ControlToValidate="primeUserInput" ErrorMessage="Enter a valid number." ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
            <br />
            <asp:Button Text="Submit" runat="server" OnClick="PrimeClick" />
            <hr />
            <div id="primeOutput" runat="server">
            </div>
        </div>
    </form>
</body>
</html>

﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Bonus_N01331213._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">



    <div class="jumbotron">
        <h1>Bonus Assignment</h1>
        <p class="lead">Looking for quadrants, prime numbers and palindromes? You came to the right place.</p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Which Quadrant?</h2>
            <p>
                Enter two non-zero intergaers and the quadrant of those numbers in a Cartesian coordinate system will be displayed.
            </p>
            <p>
                <a class="btn btn-default" href="FindTheQuadrant.aspx">  &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Is it Prime?</h2>
            <p>
                Enter a positive interger and find out if it is a prime number or not.
            </p>
            <p>
                <a class="btn btn-default" href="PrimeOrNot.aspx">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Is it a Palindrome?</h2>
            <p>
                Enter text and find out if it is a palindrome or not.
            </p>
            <p>
                <a class="btn btn-default" href="PalindromeOrNot.aspx">Learn more &raquo;</a>
            </p>
        </div>
    </div>

</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Bonus_N01331213
{
    public partial class IsNumberPrime : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        // Some help with the logic from https://www.tutorialspoint.com/Chash-Program-to-check-if-a-number-is-prime-or-not

        protected void PrimeClick(object sender, EventArgs e)
        {
            int userNumber = int.Parse(primeUserInput.Text);

            int x = 0;
            for (int i=1; i<=userNumber; i++)
            {
                if (userNumber % i == 0)
                {
                    x++;
                }
            }

            if (x == 2)
            {
                primeOutput.InnerHtml = userNumber + " is a prime number.";
            }
            else
            {
                primeOutput.InnerHtml = userNumber + " is Not a prime number.";
            }
        }
    }
}